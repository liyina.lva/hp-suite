# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationSalesPoint(models.Model):
    _name = 'homologation.sales.point'

    name = fields.Char("Nombre")
