# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationFiscalDocumentType(models.Model):
    _name = 'homologation.fiscal.document.type'

    name = fields.Char(string='Nombre')
    mode = fields.Char(string='Modo', related='invoice_mode_id.mode', readonly=True)
    code = fields.Char(string='Código', related='invoice_mode_id.code', readonly=True)
    invoice_mode_id = fields.Many2one(comodel_name='sin.code.invoice.mode', string='Modo de factura')
    fiscal_position_ids = fields.Many2many(comodel_name='account.fiscal.position', string='Posición fiscal')
