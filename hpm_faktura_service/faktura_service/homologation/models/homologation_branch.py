# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationBranch(models.Model):
    _name = 'homologation.branch'

    def _get_branch_office_domain(self):
        return self.branch_office_id.get_standard_rol_domain('branch_office')

    name = fields.Char("Nombre")
    code = fields.Char("Código")
    address = fields.Char("Dirección")
    branch_office_id = fields.Many2one(comodel_name='res.partner', string='Sucursal', domain=_get_branch_office_domain)
