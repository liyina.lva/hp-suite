# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationEmissionType(models.Model):
    _name = 'homologation.emission.type'

    name = fields.Char(string='Nombre', related='emission_type_id.type', readonly=True)
    code = fields.Char(string='Código', related='emission_type_id.code', readonly=True)
    emission_type_id = fields.Many2one(comodel_name='sin.code.emission.type', string='Tipo de emisión')
