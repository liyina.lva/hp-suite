# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationInvoiceMode(models.Model):
    _name = 'homologation.invoice.mode'

    name = fields.Char(string='Nombre')
    code = fields.Char(string='Código')
