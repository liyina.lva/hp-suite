# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationAreaDocumentType(models.Model):
    _name = 'homologation.area.document.type'

    name = fields.Char(string='Nombre', related='sector_document_id.document_name', readonly=True)
    code = fields.Char(string='Código', related='sector_document_id.code', readonly=True)
    sector_document_id = fields.Many2one(comodel_name='sin.code.sector.document', string='Tipo de emisión')
