# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationProduct(models.Model):
    _name = 'homologation.product'

    name = fields.Char(string='Nombre', related='sin_product_id.product', readonly=True)
    code = fields.Char(string='Código', related='sin_product_id.code', readonly=True)
    code_activity = fields.Char(string='Código de actividad', related='sin_product_id.code_activity', readonly=True)
    product_ids = fields.Many2many(comodel_name='product.template', string='Productos')
    sin_product_id = fields.Many2one(comodel_name='sin.code.products', string='SIN Productos')
