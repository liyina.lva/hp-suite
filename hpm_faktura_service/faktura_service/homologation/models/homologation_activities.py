# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationActivities(models.Model):
    _name = 'homologation.activities'

    name = fields.Char(string='Nombre', related='activities_id.activity', readonly=True)
    code = fields.Char(string='Código', related='activities_id.code', readonly=True)
    activities_id = fields.Many2one(comodel_name='sin.code.activities', string='Actividad econónica')
