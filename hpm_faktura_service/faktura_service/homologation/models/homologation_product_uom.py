# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationProductUOM(models.Model):
    _name = 'homologation.product.uom'

    name = fields.Char(string='Nombre', related='sin_product_uom_id.product_uom', readonly=True)
    code = fields.Char(string='Código', related='sin_product_uom_id.code', readonly=True)
    product_uom_id = fields.Many2one(comodel_name='product.uom', string='Unidad de medida')
    sin_product_uom_id = fields.Many2one(comodel_name='sin.code.product.uom', string='SIN unidad de medida')
