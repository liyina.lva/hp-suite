# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationPaymentMethod(models.Model):
    _name = 'homologation.payment.method'

    name = fields.Char(string='Nombre', related='payment_method_id.method', readonly=True)
    code = fields.Char(string='Código', related='payment_method_id.code', readonly=True)
    payment_type = fields.Char(string='Tipo')
    payment_method_id = fields.Many2one(comodel_name='sin.code.payment.method', string='Método de pago')
