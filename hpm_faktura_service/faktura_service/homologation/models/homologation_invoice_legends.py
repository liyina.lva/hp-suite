# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationInvoiceLegends(models.Model):
    _name = 'homologation.invoice.legends'

    name = fields.Char(string='Nombre', related='invoice_legends_id.legends', readonly=True)
    code = fields.Char(string='Código', related='invoice_legends_id.code', readonly=True)
    invoice_legends_id = fields.Many2one(comodel_name='sin.code.invoice.legends', string='Leyenda de factura')
