# -*- coding: utf-8 -*-

from . import homologation_activities
from . import homologation_area_document_type
from . import homologation_branch
from . import homologation_currency
from . import homologation_emission_type
from . import homologation_fiscal_document_type
from . import homologation_invoice_legends
from . import homologation_invoice_mode
from . import homologation_payment_method
from . import homologation_product
from . import homologation_product_uom
from . import homologation_sales_point
