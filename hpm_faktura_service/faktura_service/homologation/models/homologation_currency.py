# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HomologationCurrency(models.Model):
    _name = 'homologation.currency'

    name = fields.Char(string='Nombre', related='sin_currency_id.currency_name', readonly=True)
    code = fields.Char(string='Código', related='sin_currency_id.code', readonly=True)
    currency_id = fields.Many2one(comodel_name='res.currency', string='Moneda')
    sin_currency_id = fields.Many2one(comodel_name='sin.code.currency', string='SIN Moneda')
