# -*- coding: utf-8 -*-
from odoo import api, fields, models


class SinSignificanEvent(models.Model):
    _name = 'sin.significant.event'

    name = fields.Char(string='Nombre')
