# -*- coding: utf-8 -*-
import logging
from odoo import api, fields, models
from odoo.addons.hpm_faktura_service.faktura_service.sync.sin_getting_codes import SinGettingCodes
from odoo.addons.sb_utils.sb_date import sb_date

_logger = logging.getLogger(__name__)

STATES = [('new', 'Nueva'),
          ('inactive', 'Inactiva'),
          ('active', 'Activa'),
          ('expired', 'Expirada')]


class SinCodeCuis(models.Model):
    _name = 'sin.code.cuis'

    @api.model
    def update_code_cuis_from_sin(self):
        date_time = sb_date.datetime_now_time_zone_bo()
        date_time_string = sb_date.datetime_to_string(date_time)
        companies = self.env['res.company'].search([])
        homologation_branchs = self.env['homologation.branch'].search([])
        self.request_code_cuis_from_sin(date_time_string, companies, homologation_branchs)

    def request_code_cuis_from_sin(self, date_time, companies, homologation_branchs):
        for company_id in companies:
            for homologation_branch in homologation_branchs:
                if not self.exists_sin_code_cuis(date_time, company_id, homologation_branch.code):
                    self.__update_inactive_sin_code_cuis_expire(company_id, homologation_branch.code)
                    self.create_sin_code_cuis_from_sin(company_id, homologation_branch.code)

    def exists_sin_code_cuis(self, date_time, company_id, branch_office_code):
        return self.find_sin_code_cuis_by_date(date_time, company_id, branch_office_code)

    def find_sin_code_cuis_by_date(self, date_time, company_id, branch_office_code):
        return self.search([('branch_office_code', '=', branch_office_code), ('expiration_date', '>', date_time),
                            ('company_id', '=', company_id.id), ('state', '=', 'active')])

    def __update_inactive_sin_code_cuis_expire(self, company_id, branch_office_code):
        sin_code_cuis = self.search([('company_id', '=', company_id.id),
                                     ('branch_office_code', '=', branch_office_code)])
        len(sin_code_cuis) > 0 and sin_code_cuis.update({'state': 'inactive'})

    def create_sin_code_cuis_from_sin(self, company_id, branch_office_code):
        code_cuis = self.__create_sin_code_cuis(company_id, branch_office_code)
        response_cuis = SinGettingCodes(code_cuis).request_cuis(company_id)
        if not self.__error_in_response(response_cuis):
            code_cuis.__update_sin_code_cuis(response_cuis)

    def __error_in_response(self, response):
        pass

    def __create_sin_code_cuis(self, company_id, branch_office_code):
        code_cuis = self.create({'branch_office_code': branch_office_code, 'company_id': company_id.id})
        return code_cuis

    def __update_sin_code_cuis(self, response_cuis):
        expiration_date = sb_date.datetime_to_string(response_cuis.fechaVigencia)
        self.update({'cuis': response_cuis.codigo, 'expiration_date': expiration_date, 'state': 'active'})
        _logger.info("sin code cuis:{} expiration date:{} saved".format(response_cuis.codigo,
                                                                        expiration_date))

    def get_cuis(self, date_time, company_id, branch_office_code):
        return self.find_sin_code_cuis_by_date(date_time, company_id, branch_office_code)

    cuis = fields.Char(string='CUIS')
    expiration_date = fields.Datetime(string='Fecha de Vigencia')
    invoice_mode = fields.Char('Modalidad', default='1')
    branch_office_code = fields.Char('Código de sucursal', default='0')
    sales_point_code = fields.Char('Código de punto de venta', default='0')
    company_id = fields.Many2one(comodel_name='res.company', string='Compañía',
                                 default=lambda self: self.env.user.company_id)
    state = fields.Selection(selection=STATES, string='Estado', default="new")
