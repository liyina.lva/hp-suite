# -*- coding: utf-8 -*-
import logging
from odoo import api, fields, models
from odoo.addons.hpm_faktura_service.faktura_service.sync.sin_data_sync import SinDataSync
from odoo.addons.sb_utils.sb_date import sb_date

_logger = logging.getLogger(__name__)


class SinCodeActivitiesDocumentSector(models.Model):
    _name = 'sin.code.activities.document.sector'
    _order = "code"

    @api.model
    def update_code_activities_document_sector_from_sin(self):
        date_time = sb_date.datetime_now_time_zone_bo()
        date_time_string = sb_date.datetime_to_string(date_time)
        companies = self.env['res.company'].search([])
        self.request_code_activities_document_sector_from_sin(date_time_string, companies)

    def request_code_activities_document_sector_from_sin(self, date_time, companies):
        for company_id in companies:
            if not self.exists_sin_code_activities_document_sector(date_time, company_id):
                self.__create_sin_code_activities_document_sector_from_sin(company_id)

    def exists_sin_code_activities_document_sector(self, date_time, company_id):
        pass

    def __create_sin_code_activities_document_sector_from_sin(self, company_id):
        response = SinDataSync(self).request_list_activities_document_sector(company_id)
        if not self.__error_in_response(response):
            self.__create_sin_code_activities_document_sector(response, company_id)

    def __error_in_response(self, response):
        pass

    def __create_sin_code_activities_document_sector(self, response, company_id):
        for activities in response.listaActividadesDocumentoSector:
            self.create({'code': activities.codigoActividad, 'code_document_sector': activities.codigoDocumentoSector,
                         'type_document_sector': activities.tipoDocumentoSector, 'company_id': company_id.id})
        _logger.info("sin code activities document sector:{} Compañía:{} saved".format(
            response.listaActividadesDocumentoSector, company_id.name))

    code = fields.Char(string='Código')
    code_document_sector = fields.Char(string='Código de Documento por Sector')
    type_document_sector = fields.Char(string='Tipo de Documento por Sector')
    company_id = fields.Many2one(comodel_name='res.company', string='Compañía',
                                 default=lambda self: self.env.user.company_id)
