# -*- coding: utf-8 -*-
import logging
from datetime import datetime
from odoo import api, fields, models
from odoo.addons.hpm_faktura_service.faktura_service.sync.sin_data_sync import SinDataSync
from odoo.addons.sb_utils.sb_date import sb_date

_logger = logging.getLogger(__name__)


class SinDatetime(models.Model):
    _name = 'sin.datetime'
    _order = "code"

    @staticmethod
    def get_datetime_to_string():
        date_time = sb_date.datetime_now_time_zone_bo()
        return sb_date.datetime_to_string(date_time)

    @api.model
    def update_datetime_from_sin(self):
        date_time_string = self.get_datetime_to_string()
        companies = self.env['res.company'].search([])
        self.request_datetime_from_sin(date_time_string, companies)

    def request_datetime_from_sin(self, date_time, companies):
        for company_id in companies:
            if not self.exists_sin_datetime(date_time, company_id):
                self.__create_sin_datetime_from_sin(company_id)

    def exists_sin_datetime(self, date_time, company_id):
        return self.find_sin_datetime_by_date(date_time, company_id)

    def find_sin_datetime_by_date(self, date_time, company_id):
        return self.search([('sin_datetime', '>', date_time)], limit=1)

    def __create_sin_datetime_from_sin(self, company_id):
        response_datetime = SinDataSync(self).request_datetime(company_id)
        if not self.__error_in_response(response_datetime):
            self.__create_sin_datetime(response_datetime, company_id)

    def __error_in_response(self, response):
        pass

    def __create_sin_datetime(self, response_datetime, company_id):
        sin_datetime = datetime.strptime(response_datetime.fechaHora, "%Y-%m-%dT%H:%M:%S.%f")
        self.create({'sin_datetime': sin_datetime, 'company_id': company_id.id})
        _logger.info("sin fecha:{} Compañía:{} saved".format(sin_datetime, company_id.name))

    def get_sin_datetime(self, company_id):
        date_time_string = self.get_datetime_to_string()
        if not self.exists_sin_datetime(date_time_string, company_id):
            self.__create_sin_datetime_from_sin(company_id)

        sin_datetime = self.search([], limit=1, order="sin_datetime desc")
        return sin_datetime.sin_datetime

    code = fields.Char(string='Código')
    sin_datetime = fields.Datetime(string='Fecha y hora')
    company_id = fields.Many2one(comodel_name='res.company', string='Compañía',
                                 default=lambda self: self.env.user.company_id)

