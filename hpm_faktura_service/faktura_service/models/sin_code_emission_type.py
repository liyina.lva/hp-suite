# -*- coding: utf-8 -*-
import logging
from odoo import api, fields, models
from odoo.addons.hpm_faktura_service.faktura_service.sync.sin_data_sync import SinDataSync
from odoo.addons.sb_utils.sb_date import sb_date

_logger = logging.getLogger(__name__)


class SinCodeEmissionType(models.Model):
    _name = 'sin.code.emission.type'
    _order = "code"

    @api.model
    def update_code_emission_type_from_sin(self):
        date_time = sb_date.datetime_now_time_zone_bo()
        date_time_string = sb_date.datetime_to_string(date_time)
        companies = self.env['res.company'].search([])
        self.request_code_emission_type_from_sin(date_time_string, companies)

    def request_code_emission_type_from_sin(self, date_time, companies):
        for company_id in companies:
            if not self.exists_sin_code_emission_type(date_time, company_id):
                self.__create_sin_code_emission_type_from_sin(company_id)

    def exists_sin_code_emission_type(self, date_time, company_id):
        pass

    def __create_sin_code_emission_type_from_sin(self, company_id):
        response = SinDataSync(self).request_parametric_type_emission(company_id)
        if not self.__error_in_response(response):
            self.__create_sin_code_emission_type(response, company_id)

    def __error_in_response(self, response):
        pass

    def __create_sin_code_emission_type(self, response, company_id):
        for emission_type in response.listaCodigos:
            self.create({'code': emission_type.codigoClasificador, 'type': emission_type.descripcion,
                         'company_id': company_id.id})
        _logger.info("sin code emission type:{} Compañía:{} saved".format(response.listaCodigos, company_id.name))

    code = fields.Char(string='Código')
    type = fields.Char(string='Tipo')
    company_id = fields.Many2one(comodel_name='res.company', string='Compañía',
                                 default=lambda self: self.env.user.company_id)
