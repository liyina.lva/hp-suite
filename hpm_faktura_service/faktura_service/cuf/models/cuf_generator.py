# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.addons.hpm_faktura_service.faktura_service.sync.sin_getting_codes import SinGettingCodes
from odoo.addons.sb_utils.sb_date import sb_date

DIM_NIT = 13
DIM_DATETIME_INVOICE = 17
DIM_BRANCH_OFFICE_CODE = 4
DIM_INVOICE_MODE = 1
DIM_EMISSION_TYPE = 1
DIM_FISCAL_DOCUMENT_TYPE = 1
DIM_AREA_DOCUMENT_TYPE = 2
DIM_INVOICE_NUMBER = 10
DIM_SALES_POINT_NUMBER = 4
DIM_BASE_11 = 1


class CufGenerator(models.Model):
    _name = 'cuf.generator'

    def pass_invoice_values(self, invoice_generator):
        self.nit = self.__format_dim_nit(invoice_generator.company_id.vat)
        datetime_string = sb_date.string_to_datetime(invoice_generator.datetime_invoice)
        self.datetime_invoice = self.__format_dim_datetime(datetime_string.strftime("%Y%m%d%H%M%S000"))
        self.branch_office_code = self.__format_dim_branch_office_code(invoice_generator.branch_office_code)
        self.invoice_mode = self.__format_dim_invoice_mode(invoice_generator.invoice_mode)
        self.emission_type = self.__format_dim_emission_type(invoice_generator.emission_type)
        self.fiscal_document_type = self.__format_dim_fiscal_document_type(invoice_generator.fiscal_document_type)
        self.area_document_type = self.__format_dim_area_document_type(invoice_generator.area_document_type)
        self.invoice_number = self.__format_dim_invoice_number(invoice_generator.invoice_number)
        self.sales_point_code = self.__format_dim_sales_point_code(invoice_generator.sales_point_code)
        # self.filtered(lambda m: m.production_id or m.raw_material_production_id) \
        #     .mapped('move_line_ids') \
        #     .filtered(lambda ml: ml.qty_done == 0.0)

    def __format_dim_nit(self, nit):
        return self.__format_dim_value(nit, DIM_NIT)

    def __format_dim_datetime(self, datetime_invoice):
        return self.__format_dim_value(datetime_invoice, DIM_DATETIME_INVOICE)

    def __format_dim_branch_office_code(self, branch_office_code):
        return self.__format_dim_value(branch_office_code, DIM_BRANCH_OFFICE_CODE)

    def __format_dim_invoice_mode(self, invoice_mode):
         return self.__format_dim_value(invoice_mode, DIM_INVOICE_MODE)

    def __format_dim_emission_type(self, emission_type):
        return self.__format_dim_value(emission_type, DIM_EMISSION_TYPE)

    def __format_dim_fiscal_document_type(self, fiscal_document_type):
        return self.__format_dim_value(fiscal_document_type, DIM_FISCAL_DOCUMENT_TYPE)

    def __format_dim_area_document_type(self, area_document_type):
        return self.__format_dim_value(area_document_type, DIM_AREA_DOCUMENT_TYPE)

    def __format_dim_invoice_number(self, invoice_number):
        return self.__format_dim_value(invoice_number, DIM_INVOICE_NUMBER)

    def __format_dim_sales_point_code(self, sales_point_code):
        return self.__format_dim_value(sales_point_code, DIM_SALES_POINT_NUMBER)

    @staticmethod
    def __format_dim_value(value, dim):
        cero_left = ''
        for n in range(0, dim-len(value)):
            cero_left += '0'
        return cero_left + value

    def onclick_generate_cuf(self):
        self.cuf = self.generate_cuf()

    def generate_cuf(self):
        concatenated_cuf_values = self.__concatenate_cuf_values()
        self.base11_self_verifier_code = self._get_modulo11(concatenated_cuf_values)
        concat_sanitized_and_mod11_digit = concatenated_cuf_values + self.base11_self_verifier_code
        concat_sanitized_and_mod11_hex = self.__convert_string_to_hex16(concat_sanitized_and_mod11_digit)
        self.cufd_control_code = self.__get_code_cufd()
        return concat_sanitized_and_mod11_hex + self.cufd_control_code

    def __get_code_cufd(self):
        date_time = sb_date.date_now_time_zone_bo()
        date_time_string = sb_date.datetime_to_string(date_time)
        company_id = self.env.user.company_id
        branch_office_code = str(int(self.branch_office_code))
        sin_code_cufd_models = self.env['sin.code.cufd']
        if not sin_code_cufd_models.exists_sin_code_cufd(date_time_string, company_id, branch_office_code):
            homologation_branchs = self.env['homologation.branch'].search([('code', '=', branch_office_code)])
            sin_code_cufd_models.request_code_cufd_from_sin(date_time_string, company_id, homologation_branchs)

        cufd_models = sin_code_cufd_models.find_sin_code_cufd_by_date(date_time_string, company_id, branch_office_code)
        cufd_control_code = cufd_models.control_code
        self.cufd = cufd_models.cufd

        return cufd_control_code

    def __concatenate_cuf_values(self):
        return self.nit + self.datetime_invoice + self.branch_office_code + self.invoice_mode + self.emission_type + \
               self.fiscal_document_type + self.area_document_type + self.invoice_number + self.sales_point_code

    def _get_modulo11(self, concatenated_cuf_values):
        return self.__calculates_digit_mod11(concatenated_cuf_values, 1, 9, False)

    @staticmethod
    def __calculates_digit_mod11(concatenated_cuf_values, num_dig, lim_mult, x10):
        dig = 0
        if not x10:
            num_dig = 1
        for n in range(0, num_dig):
            suma = 0
            mult = 2
            for i in reversed(range(0, len(concatenated_cuf_values))):
                suma += (mult * int(concatenated_cuf_values[i: i+1]))
                if ++mult > lim_mult:
                    mult = 2
                if x10:
                    dig = ((suma * 10) % 11) % 10
                else:
                    dig = suma % 11
        if dig == 10:
            concatenated_cuf_values += "1"
        if dig == 11:
            concatenated_cuf_values += "0"
        if dig < 10:
            concatenated_cuf_values += str(dig)
        return concatenated_cuf_values[len(concatenated_cuf_values) - num_dig:len(concatenated_cuf_values)]

    @staticmethod
    def __convert_string_to_hex16(concat_sanitized_and_mod11_digit):
        big_int_for_base16 = int(concat_sanitized_and_mod11_digit)
        hex_value = str(hex(big_int_for_base16)).upper()
        return hex_value[2:len(hex_value)]

    cuf = fields.Char(string='CUF')
    nit = fields.Char(string='NIT')
    datetime_invoice = fields.Char('Fecha de factura')
    branch_office_code = fields.Char('Código de sucursal')
    invoice_mode = fields.Char('Modo de factura')
    emission_type = fields.Char('Tipo de emisión')
    fiscal_document_type = fields.Char('Tipo de documento fiscal')
    area_document_type = fields.Char('Tipo de documento de area')
    invoice_number = fields.Char('Número de factura')
    sales_point_code = fields.Char('Código de punto de venta')
    base11_self_verifier_code = fields.Char('Base11 código de verificación', default='0')
    cufd = fields.Char(string='CUFD')
    cufd_control_code = fields.Char(string='Código de control del CUFD')
    origin_model_name = fields.Char(string="Nombre modelo de origen")
    origin_model_id = fields.Integer(string="Id de modelo de origen")
