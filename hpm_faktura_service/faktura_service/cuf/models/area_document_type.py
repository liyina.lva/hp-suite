# -*- coding: utf-8 -*-

from odoo import api, fields, models

class area_document_type(models.Model):
    _name = 'area.document.type'
    _order = "code"

    name = fields.Char(string='Nombre')
    code = fields.Char(string='Code')
