# -*- coding: utf-8 -*-

import xmlschema
import xml.etree.ElementTree as ET
from lxml import etree as lxml_ET, objectify
import gzip
import shutil
import hashlib
import base64
import signxml
from signxml import XMLSigner, XMLVerifier

INVOICE_XSD = '/Users/orquidea/Projects/hpmedical/hpsuite/hpm_faktura_service/faktura_service/data/xsd/compra_venta_xsd/facturaElectronicaCompraVenta.xsd'
INVOICE_XML = '/Users/orquidea/Projects/hpmedical/hpsuite/hpm_faktura_service/faktura_service/data/xsd/compra_venta_xml/facturaElectronicaCompraVenta.xml'
INVOICE_DIR = '/Users/orquidea/Projects/hpmedical/hpsuite/hpm_faktura_service/faktura_service/data/xsd/invoice_xml_generated/'
CRS_PRIVATEKEY_DIR = '/Users/orquidea/Projects/hpmedical/hpsuite/hpm_faktura_service/faktura_service/data/crs_documents/privatekey.pem'
CRS_HPMEDICAL_DIR = '/Users/orquidea/Projects/hpmedical/hpsuite/hpm_faktura_service/faktura_service/data/crs_documents/hpmedical.csr'
PEM_HPMEDICAL_DIR = '/Users/orquidea/Projects/hpmedical/hpsuite/hpm_faktura_service/faktura_service/data/crs_documents/hpmedical.pem'

from odoo import api, fields, models
from odoo.addons.hpm_faktura_service.faktura_service.sync.sin_purchase_sale_invoice import SinPurchaseSaleInvoice
from odoo.addons.sb_utils.sb_date import sb_date

STATES = [
    ('draft', 'Nuevo'),
    ('sync', 'Sincronizado'),
    ('error', 'Error')]


class InvoiceGenerator(models.Model):
    _name = 'invoice.generator'

    def onclick_generate_invoice(self):
        self._prepare_invoice()

    def _prepare_invoice(self):
        schema_xsd = self._prepare_schema()
        invoice_xml_crs = self._invoice_xml_generate()
        xml_validate = schema_xsd.is_valid(invoice_xml_crs)

        if xml_validate:
            self.file_invoice = self._invoice_zip_generate(invoice_xml_crs)
            self.hash_file = self._generate_hash_archivo(self.file_invoice)
            SinPurchaseSaleInvoice(self).request_invoice()
            # self.state = 'sync'

    @staticmethod
    def _prepare_schema():
        return xmlschema.XMLSchema(INVOICE_XSD)

    def _get_cuf(self):
        self.cuf_id = self.env['cuf.generator'].create({})
        self.cuf_id.pass_invoice_values(self)
        self.cuf_id.cuf = self.cuf_id.generate_cuf()
        return self.cuf_id.cuf

    def _invoice_xml_generate(self):
        cuf_code = self._get_cuf()
        invoice_xml_crs = INVOICE_DIR + cuf_code + '_crs.xml'
        self._invoice_xml_without_crs_generate(invoice_xml_crs)
        return invoice_xml_crs

    def _invoice_xml_without_crs_generate(self, invoice_xml_path):
        ET.register_namespace("", "http://www.w3.org/2000/09/xmldsig#")
        invoice_tree = ET.parse(INVOICE_XML.encode('utf8'))
        invoice_root = invoice_tree.getroot()
        self._set_data_invoice_header(invoice_root)
        self._set_data_invoice_detail(invoice_root)
        self._invoice_xml_w_crs_generate(invoice_root, invoice_xml_path)
        self._invoice_xml_remove_xmlns(invoice_xml_path)

    def get_homologation_payment_method(self, payment_type):
        homologation_payment_method = self.env['homologation.payment.method'].search(
            [('payment_type', '=', payment_type)], limit=1)
        return homologation_payment_method.code if len(homologation_payment_method) else ''

    def get_homologation_currency(self, currency_id):
        homologation_currency = self.env['homologation.currency'].search(
            [('currency_id', '=', currency_id.id)], limit=1)
        return homologation_currency.code if len(homologation_currency) else ''

    def get_homologation_invoice_legends(self):
        homologation_invoice_legends = self.env['homologation.invoice.legends'].search(
            [('code', '=', '464300')], limit=1)
        return homologation_invoice_legends.name if len(homologation_invoice_legends) else ''

    def get_homologation_activities(self):
        homologation_activities = self.env['homologation.activities'].search(
            [('code', '=', '464300')], limit=1)
        return homologation_activities.code if len(homologation_activities) else ''

    def get_homologation_products(self, product_id):
        code_activity = self.get_homologation_activities()
        products = self.env['homologation.product'].search(
            [('code_activity', '=', code_activity)], limit=1)
        homologation_products = products.filtered(lambda im: product_id.product_tmpl_id in im.product_ids)
        return homologation_products.code if len(homologation_products) else ''

    def get_homologation_product_uom(self, uom_id):
        homologation_product_uom = self.env['homologation.product.uom'].search(
            [('product_uom_id', '=', uom_id.id)], limit=1)
        return homologation_product_uom.code if len(homologation_product_uom) else ''

    def _set_data_invoice_header(self, invoice_root):
        datetime_string = sb_date.string_to_datetime(self.datetime_invoice)
        invoice_root.find('.//cabecera/nitEmisor').text = self.nit
        invoice_root.find('.//cabecera/razonSocialEmisor').text = self.company_id.name
        invoice_root.find('.//cabecera/municipio').text = self.invoice_id.branch_office_id.branch_office_name_for_invoice
        invoice_root.find('.//cabecera/telefono').text = self.invoice_id.branch_office_id.contact_mechanism_line_ids.filtered(lambda cml: cml.type_name == 'Teléfono').contact_mechanism_id.display_value
        invoice_root.find('.//cabecera/numeroFactura').text = self.invoice_id.invoice_number
        invoice_root.find('.//cabecera/cuf').text = self.cuf
        invoice_root.find('.//cabecera/cufd').text = self.cufd
        invoice_root.find('.//cabecera/codigoSucursal').text = self.branch_office_code
        invoice_root.find('.//cabecera/direccion').text = self.invoice_id.branch_office_id.contact_mechanism_line_ids.filtered(lambda cml: cml.type_name == 'Dirección').contact_mechanism_id.display_value
        invoice_root.find('.//cabecera/codigoPuntoVenta').text = self.sales_point_code
        invoice_root.find('.//cabecera/fechaEmision').text = datetime_string.strftime("%Y-%m-%dT00:00:00.000")
        invoice_root.find('.//cabecera/nombreRazonSocial').text = self.invoice_id.partner_name
        invoice_root.find('.//cabecera/codigoTipoDocumentoIdentidad').text = self.fiscal_document_type
        invoice_root.find('.//cabecera/numeroDocumento').text = self.invoice_id.partner_nit
        invoice_root.find('.//cabecera/complemento').text = ''
        invoice_root.find('.//cabecera/codigoCliente').text = '51158891' # Codigo del cliente?
        invoice_root.find('.//cabecera/codigoMetodoPago').text = self.get_homologation_payment_method(self.invoice_id.payment_type)
        invoice_root.find('.//cabecera/numeroTarjeta').text = '0'
        invoice_root.find('.//cabecera/montoTotal').text = str(int(self.invoice_id.total_amount))
        invoice_root.find('.//cabecera/montoTotalSujetoIva').text = str(int(self.invoice_id.total_taxable_amount))
        invoice_root.find('.//cabecera/codigoMoneda').text = self.get_homologation_currency(self.invoice_id.currency_id)
        invoice_root.find('.//cabecera/tipoCambio').text = str(int(self.invoice_id.currency_id.rate))
        invoice_root.find('.//cabecera/montoTotalMoneda').text = str(int(self.invoice_id.total_taxable_amount))
        invoice_root.find('.//cabecera/montoGiftCard').text = ''
        invoice_root.find('.//cabecera/descuentoAdicional').text = str(int(self.invoice_id.total_discount_amount))
        invoice_root.find('.//cabecera/codigoExcepcion').text = str(int(self.invoice_id.total_tax_exempt))
        invoice_root.find('.//cabecera/cafc').text = ''
        invoice_root.find('.//cabecera/leyenda').text = self.get_homologation_invoice_legends()
        invoice_root.find('.//cabecera/usuario').text = self.invoice_id.seller_partner_id.name
        invoice_root.find('.//cabecera/codigoDocumentoSector').text = self.area_document_type

    def _set_data_invoice_detail(self, invoice_root):
        invoice_detail_len = len(self.invoice_id.invoice_line_ids)
        self._add_detail_xml(invoice_root, invoice_detail_len)
        for detail_index in range(1, invoice_detail_len + 1):
            invoice_line = self.invoice_id.invoice_line_ids[invoice_detail_len - 1]
            detail_xml = invoice_root[detail_index]
            detail_xml.find('.//actividadEconomica').text = self.get_homologation_activities()
            detail_xml.find('.//codigoProductoSin').text = self.get_homologation_products(invoice_line.product_id)
            detail_xml.find('.//codigoProducto').text = invoice_line.product_id.default_code
            detail_xml.find('.//descripcion').text = invoice_line.product_id.display_name
            detail_xml.find('.//cantidad').text = str(int(invoice_line.quantity))
            detail_xml.find('.//unidadMedida').text = self.get_homologation_product_uom(invoice_line.uom_id)
            detail_xml.find('.//precioUnitario').text = str(int(invoice_line.price_unit))
            detail_xml.find('.//montoDescuento').text = str(int(invoice_line.invoice_discount_amount))
            detail_xml.find('.//subTotal').text = str(int(invoice_line.price_total))
            detail_xml.find('.//numeroSerie').text = ''
            detail_xml.find('.//numeroImei').text = ''
            invoice_detail_len = invoice_detail_len - 1

    @staticmethod
    def _add_detail_xml(invoice_root, invoice_detail_len):
        for index in range(0, invoice_detail_len):
            xml_node = ET.Element("detalle")
            ET.SubElement(xml_node, 'actividadEconomica')
            ET.SubElement(xml_node, 'codigoProductoSin')
            ET.SubElement(xml_node, 'codigoProducto')
            ET.SubElement(xml_node, 'descripcion')
            ET.SubElement(xml_node, 'cantidad')
            ET.SubElement(xml_node, 'unidadMedida')
            ET.SubElement(xml_node, 'precioUnitario')
            ET.SubElement(xml_node, 'montoDescuento')
            ET.SubElement(xml_node, 'subTotal')
            ET.SubElement(xml_node, 'numeroSerie')
            ET.SubElement(xml_node, 'numeroImei')
            invoice_root.insert(index + 1, xml_node)

    def indent(self, elem, level=0):
        i = "\n" + level * "    "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "    "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.indent(elem, level + 1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def _invoice_xml_remove_xmlns(self, invoice_crs_path):
        # ET.ElementTree(invoice_root).write(invoice_crs_path, xml_declaration=True, encoding='UTF-8')
        parser = lxml_ET.XMLParser(remove_blank_text=True, encoding='utf-8')
        tree = lxml_ET.parse(invoice_crs_path, parser)
        root_w_xmlns = tree.getroot()
        for elem in root_w_xmlns.getiterator():
            if not hasattr(elem.tag, 'find'):
                continue  # guard for Comment tags
            i = elem.tag.find('}')
            if i >= 0:
                elem.tag = elem.tag[i + 1:]
        objectify.deannotate(root_w_xmlns, cleanup_namespaces=True)
        signature = root_w_xmlns.find('.//Signature')
        signature.set('xmlns', 'http://www.w3.org/2000/09/xmldsig#')
        self.indent(root_w_xmlns)
        tree.write(invoice_crs_path, xml_declaration=True, encoding='UTF-8')

    def _invoice_xml_w_crs_generate(self, invoice_root, invoice_crs_path):
        signed_root = self._generate_crs(invoice_root, invoice_crs_path)
        ET.ElementTree(signed_root).write(invoice_crs_path, xml_declaration=True, encoding='UTF-8')

    @staticmethod
    def _generate_crs(invoice_root, invoice_crs_path):
        key = open(CRS_PRIVATEKEY_DIR, 'rt').read().encode('UTF-8')
        cert = open(CRS_HPMEDICAL_DIR, 'rt').read().encode('UTF-8')
        pem = open(PEM_HPMEDICAL_DIR, 'rt').read()
        signer = XMLSigner(method=signxml.methods.enveloped, signature_algorithm='rsa-sha256',
                                digest_algorithm='sha256',
                                c14n_algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315")

        signed_root = signer.sign(invoice_root, key=key, cert=cert)
        data_serialized = lxml_ET.tostring(signed_root, xml_declaration=True, encoding='UTF-8')
        XMLVerifier().verify(data_serialized, x509_cert=pem)
        return signed_root

    def _invoice_zip_generate(self, invoice_xml_generate):
        with open(invoice_xml_generate, 'rb') as f_in:
            with gzip.open(INVOICE_DIR + self.cuf + '_crs.zip', 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        with open(INVOICE_DIR + self.cuf + '_crs.zip', "rb") as f:
            invoice_zip_bytes = f.read()
            invoice_zip_generate = base64.b64encode(invoice_zip_bytes)
        return invoice_zip_generate

    @staticmethod
    def _generate_hash_archivo(invoice_xml_generate):
        encoded = invoice_xml_generate.encode("UTF-8")
        sha256 = hashlib.sha256(encoded)
        return sha256.hexdigest()

    def get_homologation_branch(self, branch_office_id):
        homologation_branch = self.env['homologation.branch'].search(
            [('branch_office_id', '=', branch_office_id.id)], limit=1)
        return homologation_branch.code if len(homologation_branch) else ''

    def get_homologation_invoice_mode(self, emission_type):
        homologation_invoice_mode = self.env['homologation.invoice.mode'].search(
            [('name', '=', 'Electrónica')], limit=1)
        return homologation_invoice_mode.code if len(homologation_invoice_mode) else ''

    def get_homologation_emission_type(self):
        homologation_emission_type = self.env['homologation.emission.type'].search(
            [('name', '=', 'EN LINEA')], limit=1)
        return homologation_emission_type.code if len(homologation_emission_type) else ''

    def get_homologation_fiscal_document_type(self, fiscal_position_id):
        invoice_mode = self.env['homologation.fiscal.document.type'].search([])
        homologation_fiscal_document_type = invoice_mode.filtered(lambda im: fiscal_position_id in im.fiscal_position_ids)
        return homologation_fiscal_document_type.code if len(homologation_fiscal_document_type) else ''

    def get_homologation_area_document_type(self):
        homologation_area_document_type = self.env['homologation.area.document.type'].search(
            [('name', '=', 'FACTURA COMPRA-VENTA')], limit=1)
        return homologation_area_document_type.code if len(homologation_area_document_type) else ''

    @api.multi
    @api.onchange('invoice_id')
    def _pass_invoice_values(self):
        for invoice_generator in self:
            if invoice_generator.invoice_id:
                invoice_generator.datetime_invoice = invoice_generator.invoice_id.date_invoice
                invoice_generator.branch_office_code = self.get_homologation_branch(invoice_generator.invoice_id.branch_office_id)
                invoice_generator.invoice_mode = self.get_homologation_invoice_mode(invoice_generator.invoice_id.emission_type)
                invoice_generator.emission_type = self.get_homologation_emission_type()
                invoice_generator.fiscal_document_type = self.get_homologation_fiscal_document_type(invoice_generator.invoice_id.fiscal_position_id)
                invoice_generator.area_document_type = self.get_homologation_area_document_type()
                invoice_generator.invoice_number = invoice_generator.invoice_id.invoice_number

    nit = fields.Char(string='NIT', default=lambda self: self.env.user.company_id.vat)
    datetime_invoice = fields.Datetime('Fecha de factura')
    branch_office_code = fields.Char('Código de sucursal', default='0')
    invoice_mode = fields.Char('Modo de factura', default='1')
    emission_type = fields.Char('Tipo de emisión', default='1')
    fiscal_document_type = fields.Char('Tipo de documento fiscal', default='1')
    area_document_type = fields.Char('Tipo de documento de area', default='1')
    invoice_number = fields.Char('Número de factura')
    sales_point_code = fields.Char('Código de punto de venta', default='0')
    file_invoice = fields.Char('Factura que es enviada para su validación')
    hash_file = fields.Char('Sha256 de la cadena Archivo que se envía')
    cuf_id = fields.Many2one(comodel_name='cuf.generator', string='CUF')
    cuf = fields.Char(string='CUF', related='cuf_id.cuf', readonly=True)
    cufd = fields.Char(string='CUFD', related='cuf_id.cufd', readonly=True)
    origin_model_name = fields.Char(string="Nombre modelo de origen")
    origin_model_id = fields.Integer(string="Id de modelo de origen")
    invoice_id = fields.Many2one(comodel_name='account.invoice', string='Factura')
    company_id = fields.Many2one(comodel_name='res.company', string='Compañía',
                                 default=lambda self: self.env.user.company_id)
    state = fields.Selection(string='Estado', selection=STATES, default='draft')
