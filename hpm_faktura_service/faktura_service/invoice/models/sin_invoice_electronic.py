# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.addons.hpm_faktura_service.faktura_service.sync.sin_service_invoice import SinServiceInvoice


class SinInvoiceElectronic(models.Model):
    _name = 'sin.invoice.electronic'

    def generate_invoice(self):
        SinServiceInvoice(self).request_cufd()

    nit = fields.Char(string='NIT')
    datetime_invoice = fields.Char('Fecha de factura')
    branch_office_code = fields.Char('Código de sucursal')
    invoice_mode = fields.Char('Modo de factura')
    emission_type = fields.Char('Tipo de emisión')
    fiscal_document_type = fields.Char('Tipo de documento fiscal')
    area_document_type = fields.Char('Tipo de documento de area')
    invoice_number = fields.Char('Número de factura')
    sales_point_code = fields.Char('Código de punto de venta')
    cuf_id = fields.Many2one(comodel_name='cuf.generator', string='CUF')
    cuf = fields.Char(string='CUF', related='cuf_id.cuf', readonly=True)
    cufd = fields.Char(string='CUFD', related='cuf_id.cufd', readonly=True)
    origin_model_name = fields.Char(string="Nombre modelo de origen")
    origin_model_id = fields.Integer(string="Id de modelo de origen")
