# -*- coding: utf-8 -*-
import logging
from odoo.addons.hpm_faktura_service.faktura_service.sync.soap_service import SoapService
from odoo.addons.hpm_faktura_service.faktura_service.sync.sin_getting_codes import SinGettingCodes
from odoo.addons.sb_utils.sb_date import sb_date

_logger = logging.getLogger(__name__)


class SinPurchaseSaleInvoice(object):

    def __init__(self, faktura_service):
        self.models = faktura_service
        self.env = faktura_service.env
        self.url = self.env['wara.config'].get_string('hpm_faktura_service.sin_purchase_sale_invoice_wsdl')
        self.environment_code = self.env['wara.config'].get_int('hpm_faktura_service.sin_code_action')
        self.delegate_token = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_delegate_token')
        self.system_code = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_cs')

    def sin_request_service(self):
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        return SoapService.get_service(self.url, self.delegate_token)

    def request_invoice(self):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = SoapService.get_service(self.url, self.delegate_token)
        if client.service.verificarComunicacion():
            response = self.sin_request_invoice(client)
        _logger.debug('response {}'.format(response))
        return response

    def sin_request_invoice(self, client):
        cuis_code = SinGettingCodes(self.models).get_sin_code_cuis(self.models.company_id)
        cufd_code = SinGettingCodes(self.models).get_sin_code_cufd(self.models.company_id)
        sin_datetime = self.env['sin.datetime'].get_sin_datetime(self.models.company_id)
        datetime_string = sb_date.string_to_datetime(sin_datetime)
        request_receipt_invoice = client.factory.create('ns0:solicitudRecepcionFactura')
        request_receipt_invoice.codigoAmbiente = self.environment_code
        request_receipt_invoice.codigoDocumentoSector = int(self.models.area_document_type)
        request_receipt_invoice.codigoEmision = int(self.models.emission_type)
        request_receipt_invoice.codigoModalidad = int(self.models.invoice_mode)
        request_receipt_invoice.codigoPuntoVenta = int(self.models.sales_point_code)
        request_receipt_invoice.codigoSistema = self.system_code
        request_receipt_invoice.codigoSucursal = int(self.models.branch_office_code)
        request_receipt_invoice.cufd = cufd_code
        request_receipt_invoice.cuis = cuis_code
        request_receipt_invoice.nit = int(self.models.company_id.vat)
        request_receipt_invoice.tipoFacturaDocumento = int(self.models.fiscal_document_type)
        request_receipt_invoice.archivo = self.models.file_invoice
        request_receipt_invoice.fechaEnvio = datetime_string.strftime("%Y-%m-%dT%H:%M:%S.000")
        request_receipt_invoice.hashArchivo = self.models.hash_file
        response = client.service.recepcionFactura(request_receipt_invoice)
        return response
