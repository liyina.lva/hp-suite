# -*- coding: utf-8 -*-
import logging

from odoo.addons.hpm_faktura_service.faktura_service.sync.soap_service import SoapService
from odoo.addons.sb_utils.sb_date import sb_date
_logger = logging.getLogger(__name__)


class SinDataSync(object):

    def __init__(self, faktura_service):
        self.env = faktura_service.env
        self.url = self.env['wara.config'].get_string('hpm_faktura_service.sin_data_sync_wsdl')
        self.environment_code = self.env['wara.config'].get_int('hpm_faktura_service.sin_code_action')
        self.delegate_token = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_delegate_token')
        self.system_code = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_cs')
        self.nit = self.env.user.company_id.vat
        self.branch_code = 0
        self.point_of_sale_code = 0

    def sin_request_service(self):
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        return SoapService.get_service(self.url, self.delegate_token)

    def __get_sin_code_cuis(self, company_id):
        date_time = sb_date.date_now_time_zone_bo()
        date_time_string = sb_date.datetime_to_string(date_time)
        sin_code_cuis_models = self.env['sin.code.cuis']
        if not sin_code_cuis_models.exists_sin_code_cuis(date_time_string, company_id, self.branch_code):
            homologation_branchs = self.env['homologation.branch'].search([('code', '=', self.branch_code)])
            sin_code_cuis_models.request_code_cuis_from_sin(date_time_string, company_id, homologation_branchs)

        cuis_models = sin_code_cuis_models.find_sin_code_cuis_by_date(date_time_string, company_id, self.branch_code)
        return cuis_models.cuis

    def sin_sync_request(self, client, company_id):
        cuis = self.__get_sin_code_cuis(company_id)
        sync_request = client.factory.create('ns0:solicitudSincronizacion')
        sync_request.codigoAmbiente = self.environment_code
        sync_request.codigoSistema = self.system_code
        sync_request.nit = int(company_id.vat)
        sync_request.cuis = cuis
        sync_request.codigoPuntoVenta = self.point_of_sale_code
        sync_request.codigoSucursal = self.branch_code
        # TODO: Validar los campos a enviar
        return sync_request

    def request_code_activities(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarActividades(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_datetime(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarFechaHora(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_list_activities_document_sector(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarListaActividadesDocumentoSector(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_list_legends_invoice(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarListaLeyendasFactura(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_list_soap_messages(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarListaMensajesServicios(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_list_products_service(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarListaProductosServicios(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_significant_events(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaEventosSignificativos(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_motive_return(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaMotivoAnulacion(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_country_origin(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaPaisOrigen(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_type_document_identity(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaTipoDocumentoIdentidad(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_document_type_sector(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaTipoDocumentoSector(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_type_emission(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaTipoEmision(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_type_room(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaTipoHabitacion(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_type_payment_method(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaTipoMetodoPago(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_type_currency(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaTipoMoneda(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_type_point_sale(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaTipoPuntoVenta(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_invoice_type(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaTiposFactura(sync_request)
        _logger.debug('response {}'.format(response))
        return response

    def request_parametric_unit_measure(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            sync_request = self.sin_sync_request(client, company_id)
            # TODO: Atrapar error
            response = client.service.sincronizarParametricaUnidadMedida(sync_request)
        _logger.debug('response {}'.format(response))
        return response
