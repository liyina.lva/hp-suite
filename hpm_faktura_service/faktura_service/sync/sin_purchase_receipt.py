# -*- coding: utf-8 -*-
import logging

from odoo.addons.hpm_faktura_service.faktura_service.sync.soap_service import SoapService
_logger = logging.getLogger(__name__)


class SinPurchaseReceipt(object):

    def __init__(self, faktura_service):
        self.env = faktura_service.env
        self.url = self.env['wara.config'].get_string('hpm_faktura_service.sin_purchase_receipt_wsdl')
        self.environment_code = self.env['wara.config'].get_int('hpm_faktura_service.sin_code_action')
        self.delegate_token = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_delegate_token')
        self.system_code = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_cs')
        self.nit = self.env.user.company_id.vat
        self.modality_code = 1
        self.branch_code = 0
        self.point_of_sale_code = 0

    def sin_request_service(self):
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        return SoapService.get_service(self.url, self.delegate_token)

    def request_cuis(self):
        response = ''
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            response = self.sin_request_cuis(client)
        return response

    def sin_request_cuis(self, client):
        code_cuis = client.factory.create('ns0:cuis')
        code_cuis.SolicitudCuis.codigoAmbiente = self.environment_code
        code_cuis.SolicitudCuis.codigoModalidad = self.modality_code
        code_cuis.SolicitudCuis.codigoPuntoVenta = self.point_of_sale_code
        code_cuis.SolicitudCuis.codigoSistema = self.system_code
        code_cuis.SolicitudCuis.codigoSucursal = self.branch_code
        code_cuis.SolicitudCuis.nit = int(self.nit)
        response = client.service.cuis(code_cuis.SolicitudCuis)
        return response

    def request_cufd(self):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = SoapService.get_service(self.url, self.delegate_token)
        if client.service.verificarComunicacion():
            response = self.sin_request_cufd(client)
        _logger.debug('response {}'.format(response))
        return response

    def sin_request_cufd(self, client):
        cuis_models = self.env['sin.code.cuis'].search([('state', '=', 'active')])
        code_cufd = client.factory.create('ns0:cufd')
        code_cufd.SolicitudCufd.codigoAmbiente = self.environment_code
        code_cufd.SolicitudCufd.codigoModalidad = self.modality_code
        code_cufd.SolicitudCufd.codigoPuntoVenta = self.point_of_sale_code
        code_cufd.SolicitudCufd.codigoSistema = self.system_code
        code_cufd.SolicitudCufd.codigoSucursal = self.branch_code
        code_cufd.SolicitudCufd.cuis = cuis_models.cuis
        code_cufd.SolicitudCufd.nit = int(self.nit)
        response = client.service.cufd(code_cufd.SolicitudCufd)
        return response
