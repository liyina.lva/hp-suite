# -*- coding: utf-8 -*-
import logging

import suds
from suds.client import Client

_logger = logging.getLogger(__name__)


class SoapService:

    @staticmethod
    def get_service(url, delegate_token=''):
        client = suds.client.Client(url)
        client.set_options(headers={'apikey': ('TokenApi ' + delegate_token)})
        return client
