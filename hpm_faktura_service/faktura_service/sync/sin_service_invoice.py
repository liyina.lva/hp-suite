# -*- coding: utf-8 -*-
import logging
import time
from datetime import datetime, date
from odoo.addons.hpm_faktura_service.faktura_service.sync.soap_service import SoapService
from odoo.addons.sb_utils.sb_date import sb_date

_logger = logging.getLogger(__name__)


class SinServiceInvoice(object):

    def __init__(self, faktura_service):
        self.models = faktura_service
        self.env = faktura_service.env
        self.url = self.env['wara.config'].get_string('hpm_faktura_service.sin_invoice_computerized_wsdl')
        self.environment_code = self.env['wara.config'].get_int('hpm_faktura_service.sin_code_action')
        self.delegate_token = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_delegate_token')
        self.system_code = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_cs')
        self.nit = self.env.user.company_id.vat
        self.modality_code = 2
        self.branch_code = 0
        self.point_of_sale_code = 0
        self.area_document_type = 1
        self.emission_type = 1
        self.invoice_document_type = 1
        self.datetime_invoice = datetime.now()

    def sin_request_service(self):
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        return SoapService.get_service(self.url, self.delegate_token)

    def request_invoice(self):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = SoapService.get_service(self.url, self.delegate_token)
        if client.service.verificarComunicacion():
            response = self.sin_request_invoice(client)
        _logger.debug('response {}'.format(response))
        return response

    def sin_request_invoice(self, client):
        cuis_models = self.env['sin.code.cuis'].search([('state', '=', 'active')])
        cufd_models = self.env['sin.code.cufd'].search([('state', '=', 'active')])
        cuf_models = self.env['cuf.generator'].search([('id', '=', 1)])
        request_receipt_invoice = client.factory.create('ns0:solicitudRecepcionFactura')
        request_receipt_invoice.codigoAmbiente = self.environment_code
        request_receipt_invoice.codigoDocumentoSector = self.area_document_type
        request_receipt_invoice.codigoEmision = self.emission_type
        request_receipt_invoice.codigoModalidad = self.modality_code
        request_receipt_invoice.codigoPuntoVenta = self.point_of_sale_code
        request_receipt_invoice.codigoSistema = self.system_code
        request_receipt_invoice.codigoSucursal = self.branch_code
        request_receipt_invoice.cufd = cufd_models.cufd
        request_receipt_invoice.cuis = cuis_models.cuis
        request_receipt_invoice.nit = int(self.nit)
        request_receipt_invoice.tipoFacturaDocumento = self.invoice_document_type
        request_receipt_invoice.archivo = self.models.file_invoice
        request_receipt_invoice.fechaEnvio = '2021-10-06T16:03:48.675'
        request_receipt_invoice.hashArchivo = self.models.hash_file
        response = client.service.recepcionFactura(request_receipt_invoice)
        return response

    def __format_response(self, response):
        code_error = SinServiceInvoice.__get_error_from_response(response)
        code_value = SinServiceInvoice.__get_value_from_response(response)

        if code_error == '0':
            return {'has_error': False, 'value': code_value}

        self.__print_error(code_error)
        return {'has_error': True}

    @staticmethod
    def __get_data_from_response(response, code_data):
        code = next(filter(lambda x: x.codDato == code_data, response), None)
        return code.dato

    @staticmethod
    def __get_error_from_response(response):
        return SinServiceInvoice.__get_data_from_response(response, 'CodError')

    @staticmethod
    def __get_value_from_response(response):
        return SinServiceInvoice.__get_data_from_response(response, 'Valor')

    def __print_error(self, code_error, date):
        description = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_error_' + code_error)
        _logger.warning('[{}] {} {}'.format(code_error, description, date))
