# -*- coding: utf-8 -*-
import logging

from odoo.addons.hpm_faktura_service.faktura_service.sync.soap_service import SoapService
from odoo.addons.sb_utils.sb_date import sb_date
_logger = logging.getLogger(__name__)


class SinGettingCodes(object):

    def __init__(self, faktura_service):
        self.env = faktura_service.env
        self.url = self.env['wara.config'].get_string('hpm_faktura_service.sin_getting_codes_wsdl')
        self.environment_code = self.env['wara.config'].get_int('hpm_faktura_service.sin_code_action')
        self.delegate_token = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_delegate_token')
        self.system_code = self.env['wara.config'].get_string('hpm_faktura_service.sin_code_cs')
        self.invoice_mode = faktura_service.invoice_mode
        self.branch_code = faktura_service.branch_office_code
        self.point_of_sale_code = faktura_service.sales_point_code

    def sin_request_service(self):
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        return SoapService.get_service(self.url, self.delegate_token)

    def request_cuis(self, company_id):
        response = ''
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            response = self.sin_request_cuis(client, company_id)
        return response

    def sin_request_cuis(self, client, company_id):
        code_cuis = client.factory.create('ns0:cuis')
        code_cuis.SolicitudCuis.codigoAmbiente = self.environment_code
        code_cuis.SolicitudCuis.codigoModalidad = self.invoice_mode
        code_cuis.SolicitudCuis.codigoPuntoVenta = self.point_of_sale_code
        code_cuis.SolicitudCuis.codigoSistema = self.system_code
        code_cuis.SolicitudCuis.codigoSucursal = self.branch_code
        code_cuis.SolicitudCuis.nit = int(company_id.vat)
        # TODO: Validar los campos a enviar
        # TODO: Atrapar error
        response = client.service.cuis(code_cuis.SolicitudCuis)
        return response

    def request_cufd(self, company_id):
        response = ''
        _logger.debug('request to url:{} code_action:{}'.format(self.url, self.environment_code))
        client = self.sin_request_service()
        if client.service.verificarComunicacion():
            response = self.sin_request_cufd(client, company_id)
        _logger.debug('response {}'.format(response))
        return response

    def get_sin_code_cuis(self, company_id):
        date_time = sb_date.date_now_time_zone_bo()
        date_time_string = sb_date.datetime_to_string(date_time)
        sin_code_cuis_models = self.env['sin.code.cuis']
        if not sin_code_cuis_models.exists_sin_code_cuis(date_time_string, company_id, self.branch_code):
            homologation_branchs = self.env['homologation.branch'].search([('code', '=', self.branch_code)])
            sin_code_cuis_models.request_code_cuis_from_sin(date_time_string, company_id, homologation_branchs)

        cuis_models = sin_code_cuis_models.find_sin_code_cuis_by_date(date_time_string, company_id, self.branch_code)
        return cuis_models.cuis

    def sin_request_cufd(self, client, company_id):
        cuis = self.get_sin_code_cuis(company_id)
        code_cufd = client.factory.create('ns0:cufd')
        code_cufd.SolicitudCufd.codigoAmbiente = self.environment_code
        code_cufd.SolicitudCufd.codigoModalidad = self.invoice_mode
        code_cufd.SolicitudCufd.codigoPuntoVenta = self.point_of_sale_code
        code_cufd.SolicitudCufd.codigoSistema = self.system_code
        code_cufd.SolicitudCufd.codigoSucursal = self.branch_code
        code_cufd.SolicitudCufd.cuis = cuis
        code_cufd.SolicitudCufd.nit = int(company_id.vat)
        # TODO: Validar los campos a enviar
        # TODO: Atrapar error
        response = client.service.cufd(code_cufd.SolicitudCufd)
        return response

    def get_sin_code_cufd(self, company_id):
        date_time = sb_date.date_now_time_zone_bo()
        date_time_string = sb_date.datetime_to_string(date_time)
        sin_code_cufd_models = self.env['sin.code.cufd']
        if not sin_code_cufd_models.exists_sin_code_cufd(date_time_string, company_id, self.branch_code):
            homologation_branchs = self.env['homologation.branch'].search([('code', '=', self.branch_code)])
            sin_code_cufd_models.request_code_cufd_from_sin(date_time_string, company_id, homologation_branchs)

        cufd_models = sin_code_cufd_models.find_sin_code_cufd_by_date(date_time_string, company_id, self.branch_code)
        return cufd_models.cufd
